"""_____________________________________________________________________

:PROJECT: LARA

*lara_parts setup *

:details: lara_parts setup file for installation.
         - For installation, run:
           run pip3 install .
           or  python3 setup.py install

:file:    setup.py
:authors: 

:date: (creation)          
:date: (last modification) 

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.2"

import os
import sys

from setuptools import setup, find_packages
#~ from distutils.sysconfig import get_python_lib

pkg_name = 'lara_parts'

def read(fname):
    try:
        return open(os.path.join(os.path.dirname(__file__), fname)).read()
    except IOError:
        return ''

install_requires = [] 
data_files = []
package_data = {}  #{pkg_name: ['lara_parts/templates/*.html', 'lara_parts/static/css/*.css',  ]}
    
setup(name=pkg_name,
    version=__version__,
    description='lara_parts',
    long_description=read('README.rst'),
    author='',
    author_email='',
    keywords='lab automation, projects, experiments, database, evaluation, visualisation, SiLA2, robots',
    url='https://github.com/LARAsuite/lara_parts',
    license='GPL',
    packages=find_packages(), #['lara_parts'],
    #~ package_dir={'lara_parts':'lara_parts'},
    install_requires = install_requires,
    test_suite='',
    classifiers=['Development Status :: 1 - Development',
                   'Environment :: commandline',
                   'Framework :: django',
                   'Intended Audience :: Developers',
                   'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python',
                   'Programming Language :: Python :: 3.5',
                   'Programming Language :: Python :: 3.6',
                   'Programming Language :: Python :: 3.7',
                   'Topic :: Utilities'],
    include_package_data=True,
    #~ package_data = package_data,
    data_files=data_files,
)
