"""_____________________________________________________________________

:PROJECT: LARA

*lara_parts urls *

:details: lara_parts urls module.
         - add app specific urls here

:file:    urls.py
:authors: 

:date: (creation)          
:date: (last modification) 

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.1"

from django.urls import path, include
from django.views.generic import TemplateView

# Add your lara_parts urls here.

urlpatterns = [
    # path('', TemplateView.as_view(template_name='index.html'), name='lara_parts-main-view'),
    # path('subdir/', include('.urls')),
]
